#include <linux/tty.h>
#include <linux/sched.h>
#include <asm/system.h>
#include <asm/io.h>
#define MSG_MOUSE_CLICK_L 1
#define MSG_MOUSE_CLICK_R 3
static unsigned char mouse_input_count = 0;
static unsigned char mouse_left_down;
static unsigned char mouse_right_down;
static unsigned char mouse_left_move;
static unsigned char mouse_down_move;
static unsigned char mouse_x_overflow;
static unsigned char mouse_y_overflow;
static unsigned char mouse_x_position;
static unsigned char mouse_y_position;
void readmouse(int mousecode)
{
    if(mousecode == 0xFA){
        mouse_input_count = 1;
        return;
    }
    switch(mouse_input_count)
    {
        case 1:
            mouse_left_down = (mousecode & 0x1) == 0x1;
            mouse_right_down = (mousecode & 0x2) == 0x2;
            mouse_left_move = (mousecode & 0x10) == 0x10;
            mouse_down_move = (mousecode & 0x20) == 0x20;
            if(mouse_left_down && mousecode == 9){
                post_message(MSG_MOUSE_CLICK_L);
            }
            if(mouse_right_down && mousecode == 10){
                post_message(MSG_MOUSE_CLICK_R);
            }
            mouse_input_count++;
            break;
        case 2:
            if(mouse_left_move)
                mouse_x_position += (int)(0xFFFFFF00|mousecode);
            else
                mouse_x_position += (int)(mousecode);
            if(mouse_x_position < 0) mouse_x_position = 0;
            mouse_input_count++;
            break;
        case 3:
            if(mouse_down_move)
                mouse_y_position += (int)(0xFFFFFF00|mousecode);
            else
                mouse_y_position += (int)(mousecode);
            if(mouse_y_position < 0) mouse_y_position = 0;
            mouse_input_count=1;
            break;
    }
}