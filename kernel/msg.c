#include <linux/sched.h>
#include <asm/segment.h>
int sys_timer_create(long milliseconds, int type)
{
    long jiffies = milliseconds / 10;
    user_timer *timer = (user_timer*)malloc(sizeof(user_timer));
    timer->init_jiffies = jiffies;
    timer->jiffies = jiffies;
    timer->type = type;
    timer->pid = current->pid;
    timer->next = timer_head;
    timer_head = timer;
    return 1;
}

void post_message(int type)
{
	//printk("posting message\n\n");
	if (msg_tail != msg_head - 1) {
		message msg;
		msg.mid = type;
		msg.pid = current->pid;
		msg_list[msg_tail] = msg;
		msg_tail = (msg_tail + 1) % MAX_MSG;
	}
}

void sys_get_message(message *msg) {
    message tmp;
	if(msg_tail==msg_head){
		put_fs_long(0,msg);
		return;
	}
	
	tmp = msg_list[msg_head];
	msg_list[msg_head].mid = 0;
	msg_head = (msg_head + 1) % MAX_MSG;;
	put_fs_long(tmp.mid,msg);

}